package ru.tsc.babeshko.tm.api.service.model;

import ru.tsc.babeshko.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {
}
