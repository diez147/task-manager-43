package ru.tsc.babeshko.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(String userId, String projectId);

    void removeAllByUserId(String userId);

}
