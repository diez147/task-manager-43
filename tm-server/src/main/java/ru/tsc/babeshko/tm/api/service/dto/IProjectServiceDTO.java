package ru.tsc.babeshko.tm.api.service.dto;

import ru.tsc.babeshko.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {
}