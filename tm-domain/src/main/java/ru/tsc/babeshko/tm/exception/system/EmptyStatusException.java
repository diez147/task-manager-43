package ru.tsc.babeshko.tm.exception.system;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}